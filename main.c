#include "bmp_utils.h"
#include <stdlib.h>
#include <string.h>
int main(int argc, char* argv[]){
	
	FILE* file = fopen(argv[1], "rb");
	if (file == NULL){
		printf("Couldn't open file\n");
		return 0;
	}else{
		struct image* map = malloc(sizeof(struct image));
		char* message = get_message_read(from_bmp(file, map));
		if (strcmp(message,"Successful reading!") == 0){
			show(map);
			printf("-----------------------------\n");
			map = rotate(map);
			show(map);
			fclose(file);
			file = fopen(argv[1], "wb");
			message = get_message_write(write_bmp(map, file, 0x4D42));
			printf("%s\n", message);
		}else{
			printf("%s\n", message);
		}
		free(map);
	}
	fclose(file);
	return 0;	
}
