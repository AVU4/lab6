#include <stdint.h>

struct pixel {
	unsigned char b, r, g;
};

struct image {
	uint32_t width;
	uint32_t height;
	struct pixel* data;
};
