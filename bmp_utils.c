#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "status.h"
#include "image_struct.h"
#include "bmp_struct.h"

static char* message_read[] = {
	"Successful reading!",
	"Couldn't read header",
	"Couldn't read pixels",
	"Invalid header values"
};

static char* message_write[] = {
	"Successful writing!",
	"Couldn't write header",
	"Couldn't write Bit Map"
};

char* get_message_read(enum read_status message) {
	return message_read[message];
}

char* get_message_write(enum write_status message) {
	return message_write[message];
}

enum read_status from_bmp(FILE* file, struct image* const data) {
	struct bmp_header* header = malloc(sizeof(struct bmp_header));
		
	if (fread(header, sizeof(struct bmp_header), 1, file) != 1)
		return READ_HEADER_NOT_POSSIBLE;
	fseek(file, header->bOffBits, SEEK_SET);

	if (header->bfType != 0x4D42)  return READ_INVALID_HEADER;

	if (header->biBitCount != 24) return READ_INVALID_HEADER;

	uint32_t width = header->biWidth;
	uint32_t height = header->biHeight;
	
	uint32_t size_row = width * sizeof(struct pixel);
	char rubbish[4];
	uint32_t junk_bits = size_row % 4 == 0 ? 0: 4 - size_row % 4;
	

	struct pixel* bitMap = malloc(height * width * sizeof(struct pixel));
	size_t i;
	for (i = 0; i < height; i ++){
		if (fread(bitMap + i*width, size_row, 1, file) != 1) return READ_INVALID_BITS;
	
		if (junk_bits != 0){
			fread(rubbish, junk_bits, 1, file);
		}
	}

	data->data = bitMap;
	data->width = header->biWidth;
	data->height = header->biHeight;

	free(header);
	return READ_OK;
	
}

static int32_t turn_x(double angle, int32_t old_x, int32_t old_y) {
	return round(old_x * cos(angle)+1*old_y*sin (angle));
}
static int32_t turn_y(double angle, int32_t old_x, int32_t old_y) {
	return round(-old_x * sin (angle) + old_y * cos (angle));
}

void show(struct image* in) {
	struct pixel* bitMap = in->data;
	size_t i;
	size_t j;
	for (i = 0; i < in->height; i ++){ 
		for (j = 0; j < in->width; j ++)
			printf("The pixel, Red %d, Blue %d, Green %d\n", (*(bitMap + i  * in->width + j)).r, (*(bitMap + i * in->width + j)).b, (*(bitMap + i* in->width + j)).g);
		printf("\n");
	}
}



struct image* rotate(struct image* const in) {
	struct pixel* bitMap = in->data;
	uint32_t width = in->width;
	uint32_t height = in->height;

	uint32_t centerX = width / 2;
	uint32_t centerY = height / 2;

	uint32_t rows = width;
	uint32_t columns = height;

	uint32_t old_size_row = width*sizeof(struct pixel);
	uint32_t size_row = columns * sizeof(struct pixel);

	struct pixel* newMap = malloc(rows* size_row);

	size_t i;
	size_t j;
	for (i = 0; i < height; i ++ ) {
		for (j = 0; j < width; j ++) {
			int32_t old_coordinate_x = j - centerX;
			int32_t old_coordinate_y = i - centerY;
			uint32_t new_coordinate_x = centerY + turn_x(90 * M_PI / 180,old_coordinate_x, old_coordinate_y);
			uint32_t new_coordinate_y = centerX + turn_y(90 * M_PI / 180, old_coordinate_x, old_coordinate_y);
			*(newMap + new_coordinate_y*columns + new_coordinate_x ) = *(bitMap + i *width  + j);
		}
	}

	in->width = columns;
	in->height = rows;
	in->data = newMap;
	free(bitMap);
	return in;	
}

enum write_status write_bmp(struct image* const in, FILE* file, uint16_t format) {
	struct bmp_header* header = malloc(sizeof(struct bmp_header));
	struct pixel* bitMap = in->data;
	uint32_t height = in->height;
	uint32_t width = in->width;
	uint32_t junk_bits = width * sizeof(struct pixel) % 4 == 0 ? 0 : 4 - width * sizeof(struct pixel)%4;
	char rubbish[4] = {0};
	
	header->bfType = format;
	header->bfileSize = sizeof(struct bmp_header) +  height * (width*sizeof(struct pixel) + junk_bits);
	header->bfReserved = bfReserved_DEFAULT;
	header->bOffBits = bOffBits_DEFAULT;
	header->biSize = biSize_DEFAULT;
	header->biWidth = width;
	header->biHeight = height;
	header->biPlanes = biPlanes_DEFAULT;
	header->biBitCount = biBitCount_DEFAULT;
	header->biCompression = biCompression_DEFAULT;
	header->biSizeImage = height * (width*sizeof(struct pixel) + junk_bits);
	header->biXPelsPerMeter = biXPelsPerMeter_DEFAULT;
	header->biYPelsPerMeter = biYPelsPerMeter_DEFAULT;
	header->biClrUsed = biClrUsed_DEFAULT;
	header->biClrImportant = biClrImportant_DEFAULT;

	if (fwrite(header, sizeof(struct bmp_header), 1, file) != 1) return WRITE_ERROR_HEADER;

	for (size_t i = 0; i < height; i ++) {
		if (fwrite((bitMap + i*width), sizeof(struct pixel) , width, file) != width) return WRITE_ERROR_BIT_MAP;
		if (junk_bits != 0) 
			fwrite(&rubbish, sizeof(char), junk_bits, file);
	}

	free(header);
	free(in->data);
	return WRITE_OK;
}

