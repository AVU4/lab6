compile:
	gcc -ansi -pedantic -Wall -Werror -lm -g -o  main main.c bmp_utils.c

run:
	./main 1_1.bmp
	./main 3_3.bmp
	./main Lights.bmp
	./main FLAG_B24.BMP

